class RepositoryPlugin < Noosfero::Plugin

  def self.plugin_name
    _('Repository')
  end

  def self.plugin_description
    _('A folder to store medias with related meta-data.')
  end

  def stylesheet?
    true
  end

  def content_types
    [RepositoryPlugin::Repository]
  end

  def article_extra_toolbar_buttons(page)
    if (page.is_a? RepositoryPlugin::Repository)
      {
        id: 'new-content-btn',
        class: 'button with-text icon-new',
        url: {action: 'new', controller: 'repository_plugin_content', parent_id: page.id},
        title: _("New Content"),
        icon: :new
      }
    end
  end

  def article_extra_contents(content)
    metadata_keys = {
      'Working groups'      => _('Working groups'),
      'Media type'          => _('Media type'),
      'Author'              => _('Author'),
      'Date'                => _('Date'),
      'Source'              => _('Source'),
      'Comment'             => _('Comment'),
      'Place of production' => _('Place of production'),
      'Places mentioned'    => _('Places mentioned'),
      'Names mentioned'     => _('Names mentioned')
    }
    if content.parent.is_a? RepositoryPlugin::Repository
      lambda do
        empty = content_tag 'small', _('empty'), class: 'no-metadata'
        content_tag(_('Metadata')) +
        content_tag('ul',
          metadata_keys.keys.map do |k|
            hk = metadata_keys[k].html_safe
            val = content.metadata[k]
            content_tag 'li', "<strong>#{hk}:</strong> #{val.blank? ? empty : h(val)}".html_safe
          end.join("\n").html_safe,
          id: 'repo-metadata'
        )
      end
    end
  end

end
