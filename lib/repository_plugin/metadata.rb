class RepositoryPlugin::Metadata < ApplicationRecord

  self.table_name = :repository_plugin_metadatas

  belongs_to :article
  validates_presence_of :article_id
  validates_uniqueness_of :article_id
  attr_accessible :article_id, :groups, :media_type, :author, :date, :source, :comment,
                  :place_of_production, :places_mentioned, :names_mentioned

  def content
    article
  end

end
